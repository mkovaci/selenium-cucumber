package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {

	public static WebDriver open(String browserType) {
		
		if (browserType.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver", "/usr/local/share/chromedriver");
			return new ChromeDriver();
		} else {
			
			System.setProperty("webdriver.chrome.driver", "");
			return new FirefoxDriver();
		}
	}
}
